from adapters.base import BaseAdaptor
from core.entities.zone import Zone as ZoneEntity


class ZoneAdaptor(BaseAdaptor):
    ENTITY = ZoneEntity
