from adapters.base import BaseAdaptor
from core.entities.farm import Farm as FarmEntity


class FarmAdaptor(BaseAdaptor):
    ENTITY = FarmEntity

