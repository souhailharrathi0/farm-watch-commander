from common.exceptions import FWCBaseException


class AdaptorException(FWCBaseException):
    pass
