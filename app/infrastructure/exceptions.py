from common.exceptions import FWCBaseException


class DBReposException(FWCBaseException):
    pass
